#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void PrintMadLib(string* pinput, ostream &os)
{
	os  << "One day my " << pinput[0] << " friend and I decided to go to the " << pinput[1] << " game in " <<
		pinput[2] << ".\n" << "We really wanted to see the " << pinput[3] << " play. \n"
		<< "So we " << pinput[4] << " in the " << pinput[5] << " and headed down to " << pinput[6]
		<< " and bought some " << pinput[7] << ".\n" << "We watched the game and it was " << pinput[8]
		<< ".\n" << "We ate some " << pinput[9] << " and drank some " << pinput[10] << ".\n"
		<< "We had a " << pinput[11] << " time, and can't wait to go again.";
}

int main()
{
	const string filepath = "c:/Temp/Madlib.txt";
	const int SIZE = 12; // Array Size
	char save = 'n'; // If the user wants to print the madlib to a file

	string input[SIZE] =
	{ "an adjective", "a sport", "a city name", " a person", "an action verb", "a vehicle", "a place"
	, "a noun", "an adjective", "a food", "a liquid", "an adjective"};
	// Get words from user
	for (int i = 0; i < SIZE; i++)
	{
		cout << "Enter " << input[i] <<": ";
		getline(cin, input[i]);
	}

	cout << "\n\n\n";
	// Print to screen
	PrintMadLib(input, cout);

	cout << "\n\n" << "Would you like to save to a text file?";

	ofstream ofs(filepath);
	cin >> save;
	// If Y, print to file
	if (save == 'y' || save=='Y')
	{
		PrintMadLib(input, ofs);
	}
	ofs.close();

	cout << "\nPress anykey to exit.";
	_getch();
	return 0;
}